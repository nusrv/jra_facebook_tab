<?php
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';
function get_row($col){
    $file = __DIR__.'/JRA-Campaign-Restaurants.xlsx';

    $array_columns =['A','B','C','D','E','F','G'];

    $objPHPExcel =  PHPExcel_IOFactory::load($file);
    $worksheet = $objPHPExcel->getActiveSheet();
    $maxCell = $worksheet->getHighestColumn();
    $cell_title = '';
    $row_items = [];
//        for($col = 0;$col < array_search($maxCell,$array_columns)+1;$col++) {

    $column = $col /*$array_columns[$col]*/;
    $cell_title = $worksheet->getCell($column.'1');
    $lastRow = $worksheet->getHighestRow();
    for ($row = 2; $row <= $lastRow; $row++) {
        $cell = $worksheet->getCell($column . $row);

        $row_items[trim($cell_title->getValue())][trim($cell->getValue())] = trim($cell->getValue());

    }
    return $row_items;
}
function get_row_and_arrange($col_1,$col_2,$from,$activities){


    $file = 'JRA-Campaign-Restaurants.xlsx';
    $array_columns =['A','B','C','D','E','F','G'];
    $objPHPExcel =  PHPExcel_IOFactory::load($file);
    $worksheet = $objPHPExcel->getActiveSheet();
    $maxCell = $worksheet->getHighestColumn();
    $cell_title = '';
    $row_items = [];
//        for($col = 0;$col < array_search($maxCell,$array_columns)+1;$col++) {

    $column = $col_1 /*$array_columns[$col]*/;
    $column_2 = $col_2;
    $cell_title = $worksheet->getCell($column.'1');
    $lastRow = $worksheet->getHighestRow();
    for ($row = 2; $row <= $lastRow; $row++) {
        $cell = $worksheet->getCell($column . $row);
        $cell_2 = $worksheet->getCell($column_2 . $row);
        $city = $worksheet->getCell('C' . $row);
        $activity = $worksheet->getCell('E' . $row);
        if(trim($activities) == 'المدن الترفيهية'){
            if(trim($activity->getValue()) == 'المدن الترفيهية' || trim($activity->getValue()) == 'الحدائق المائية' || trim($activity->getValue()) == 'المنتجعات') {
                $row_items[trim($cell->getValue())][] = trim($cell_2->getValue());
            }
        }else {
            if (trim($city->getValue()) == trim($from) && trim($activity->getValue()) == trim($activities)) {
                $row_items[trim($cell->getValue())][] = trim($cell_2->getValue());
            }
        }
    }
    return $row_items;
}